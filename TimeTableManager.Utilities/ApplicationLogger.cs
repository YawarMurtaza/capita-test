﻿using log4net;

namespace TimeTableManager.Utilities
{
    public class ApplicationLogger : IApplicationLogger
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ApplicationLogger));

        public void LogInfo(string message, params object[] param)
        {
            Logger.InfoFormat(message, param);
        }
    }
}