﻿namespace TimeTableManager.Utilities
{
    public interface IApplicationLogger
    {
        void LogInfo(string message, params object[] param);
    }
}