using System.Collections.Generic;
using TimeTableManager.Mvc.Web.ViewModels;
using System.Linq;
using System.Web.Mvc;
using TimeTableManager.DomainModel;
using TimeTableManager.ServiceManager;


namespace TimeTableManager.Mvc.Web.Filters
{ 
    public class LocationNamesProviderAttribute : ActionFilterAttribute
    {
        public ILocationManager LocationManager { get; set; }
        public ILessonManager LessonManager { get; set; }

        private EditLessonViewModel editModel;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            editModel = filterContext.ActionParameters["editViewModel"] as EditLessonViewModel;
            base.OnActionExecuting(filterContext);
        }
        
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            PartialViewResult result = filterContext.Result as PartialViewResult;
            if (result != null && result.Model is IHaveLocationNameSelectList){
                ((IHaveLocationNameSelectList)result.Model).AvailableLocationNames = this.AvailableLocationNames();
            }
        }



        private SelectListItem[] AvailableLocationNames()
        {
            IEnumerable<Lesson> lessons = this.LessonManager.GetAllLessonsForPeriodId(this.editModel.PeriodId, this.editModel.DayName);
            IEnumerable<string> disableRoomNames = lessons.Where(lesson => this.editModel.LessonIdsToDisable.Contains(lesson.LessonId)).Select(l => l.Location).Where(loc => !string.IsNullOrEmpty(loc));

            SelectListItem[] locationList = this.LocationManager.GetAllLocationNames().Where(loc => !string.IsNullOrEmpty(loc))
                .Select(roomName => new SelectListItem()
                                        {
                                            Text = roomName,
                                            Value = roomName
                                        }).ToArray();

            foreach (string nextNameToDisable in disableRoomNames)
            {
                locationList.First(listItem => listItem.Text == nextNameToDisable).Disabled = true;
            }

            return locationList;
        }
    }
}