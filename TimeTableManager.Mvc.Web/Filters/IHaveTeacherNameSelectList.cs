namespace TimeTableManager.Mvc.Web.Filters
{
    using System.Web.Mvc;

    public interface IHaveTeacherNameSelectList
    {
        SelectListItem[] AvailableTeachersNames { get; set; }
    }
}