using System.Linq;
using System.Web.Mvc;

using TimeTableManager.ServiceManager;

namespace TimeTableManager.Mvc.Web.Filters
{
    public class SubjectNamesProviderAttribute : ActionFilterAttribute
    {
        public ISubjectManager SubjectManager { get; set; }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            PartialViewResult result = filterContext.Result as PartialViewResult;
            if (result != null && result.Model is IHaveSubjectNameSelectList)
            {
                ((IHaveSubjectNameSelectList)result.Model).AvailableSubjectNames = this.AvailableSubjectNames();
            }
        }


        private SelectListItem[] AvailableSubjectNames()
        {
            SelectListItem[] subjectSelectList = this.SubjectManager.GetAllSubjectNames().Where(sub => !string.IsNullOrEmpty(sub))
                .Select(subjectName => new SelectListItem()
                {
                    Text = subjectName,
                    Value = subjectName
                }).ToArray();
            return subjectSelectList;
        }
    }
}