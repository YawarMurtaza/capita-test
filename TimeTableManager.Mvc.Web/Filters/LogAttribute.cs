using System.Collections.Generic;
using System.Web.Mvc;
using TimeTableManager.Utilities;

namespace TimeTableManager.Mvc.Web.Filters
{
    public class LogAttribute : ActionFilterAttribute
    {
        /// <summary> Gets or sets the text file logger. </summary>
        public IApplicationLogger FileLogger { get; set; }

        public string Description { get; set; }
        
        private IDictionary<string, object> actionParameters;

        public LogAttribute(string description)
        {
            this.Description = description;
        }

        /// <summary> Called by the ASP.NET MVC framework after the action method executes. </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            foreach (KeyValuePair<string, object> nextParam in this.actionParameters)
            {
                this.Description = this.Description.Replace("{" + nextParam.Key + "}", nextParam.Value.ToString());
            }

            string desc = this.Description;
            this.FileLogger.LogInfo("Action: {0}, Controller: {1}, Desc: {2}", actionName, controllerName, desc);
        }

        /// <summary> Called by the ASP.NET MVC framework ***BEFORE*** the action method executes. </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            this.actionParameters = filterContext.ActionParameters;
            base.OnActionExecuting(filterContext);
        }
    }
}