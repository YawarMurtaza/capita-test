namespace TimeTableManager.Mvc.Web.Filters
{
    using System.Web.Mvc;

    public interface IHaveLocationNameSelectList
    {
        SelectListItem[] AvailableLocationNames { get; set; }
    }
}