
using TimeTableManager.Mvc.Web.ViewModels;

namespace TimeTableManager.Mvc.Web.Filters
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using DomainModel;
    using ServiceManager;

    public class TeacherNamesProviderAttribute : ActionFilterAttribute
    {
        private EditLessonViewModel editModel;
        public ITeacherManager TeacherDataManager { get; set; }
        public ILessonManager LessonManager { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            editModel = filterContext.ActionParameters["editViewModel"] as EditLessonViewModel;
            base.OnActionExecuting(filterContext);
        }


        /// <summary> Called by the ASP.NET MVC framework before the action method executes. </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            PartialViewResult result = filterContext.Result as PartialViewResult;
            if (result != null && result.Model is IHaveTeacherNameSelectList)
            {
                // get the list from current http session...
                ((IHaveTeacherNameSelectList)result.Model).AvailableTeachersNames = this.AvailableTeacherNames();
            }
        }

        private SelectListItem[] AvailableTeacherNames()
        {
            IEnumerable<Lesson> lessons = this.LessonManager.GetAllLessonsForPeriodId(this.editModel.PeriodId, this.editModel.DayName).ToList();
            IEnumerable<string> disableTeacherNames = lessons.Where(l => this.editModel.LessonIdsToDisable.Contains(l.LessonId)).Select(l => l.Teacher).Select(teacher => teacher.Name).Where(teacherName => !string.IsNullOrEmpty(teacherName));

            SelectListItem[] teacherNameSelectList = this.TeacherDataManager.GetAllTeachers().Where(teacher => !string.IsNullOrEmpty(teacher.Name))
                .Select(t => new SelectListItem()
                                 {
                                     Text = t.Name,
                                     Value = t.Id.ToString()
                                 }).ToArray();

            foreach (string nextNameToDisable in disableTeacherNames)
            {
                teacherNameSelectList.First(listItem => listItem.Text == nextNameToDisable).Disabled = true;
            }
            
            return teacherNameSelectList;
        }

    }
}