using System.Web.Mvc;

namespace TimeTableManager.Mvc.Web.Filters
{
    public interface IHaveDaysOfWeekSelectList
    {
        SelectListItem[] AvailableWeekDayNames { get; set; }
    }
}