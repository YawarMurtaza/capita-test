namespace TimeTableManager.Mvc.Web.Filters
{
    using System.Web.Mvc;

    public interface IHaveSubjectNameSelectList
    {
        SelectListItem[] AvailableSubjectNames { get; set; }
    }
}