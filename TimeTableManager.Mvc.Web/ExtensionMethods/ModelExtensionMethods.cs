﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimeTableManager.DomainModel;
using TimeTableManager.Mvc.Web.ViewModels;

namespace TimeTableManager.Mvc.Web.ExtensionMethods
{
    public static class ModelExtensionMethods
    {
        public static DayOfWeek ConvertToDayOfWeekEnum(this string dayName)
        {
            return (DayOfWeek)Enum.Parse(typeof(DayOfWeek), dayName);
        }

        public static LessonDetailViewModel ConvertToLessonDetail(this Lesson lesson, int periodId)
        {
            LessonDetailViewModel ld = new LessonDetailViewModel()
            {
                LessonId = lesson.LessonId,
                TeacherName = lesson.Teacher.Name,
                Location = lesson.Location,
                SubjectName = lesson.SubjectName,
                PeriodId = periodId,
                ButtonText = string.IsNullOrEmpty(lesson.Teacher.Name) ? "Add"  : "Edit",
                ClassName = string.IsNullOrEmpty(lesson.Teacher.Name) ? "glyphicon-plus" : "glyphicon-edit",
            };
            return ld;
        }

        public static PeriodViewModel ConvertToPeriodViewModel(this Period period)
        {
            PeriodViewModel pvm = new PeriodViewModel
            {
                PeriodId = period.PeriodId,
                Lessons = period.Lessons.Select(l => l.ConvertToLessonDetail(period.PeriodId)).ToList()

            };

            return pvm;
        }

        public static IEnumerable<PeriodViewModel> ConvertToPeriodViewModel(this IEnumerable<Period> periods)
        {
            IList<PeriodViewModel> pvModels = new List<PeriodViewModel>();
            foreach (Period nextPeriod in periods)
            {
                pvModels.Add(nextPeriod.ConvertToPeriodViewModel());
            }

            return pvModels;
        }
    }
}