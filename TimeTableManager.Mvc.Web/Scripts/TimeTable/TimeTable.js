﻿
// edit button click handler...
$(document).on("click", "[data-edit-lesson-btn]", function (e) {
    e.preventDefault();

    var buttonText = $(this).text();
    

    var periodId = $(this).data("period-id");
    var lessonId = lessonId_global = $(this).data("lesson-id");
    var selectedDay = $("#schoolDaysSelectList option:selected").text();

    $("#" + lessonId).addClass("highlightSelectedLesson");

    var disableIds = [];
    $("#tr_" + periodId).find(".lessonDetail").each(function (index, data) {
        if (data.id != lessonId) {
            disableIds.push(data.id);
        }
    });

    var postData = { periodId: periodId, lessonId: lessonId, dayName: selectedDay, lessonIdsToDisable: disableIds };

    $.ajax({
        traditional: true,
        url: "/TimeTable/EditLesson/",
        data: postData,
        success: function (data) {

            $("#div_EditLesson").html(data);
            $("#div_EditLesson").dialog({
                autoOpen: true,
                width: 675,
                height: 320,
                resizable: false,
                modal: true,
                title: buttonText + " Lesson"
            });
        }
    });
});

// UPDATE lesson...
$(document).on("click", "#btnUpdateLesson", function () {
    var teacherName = $("#teacherNameSelectList option:selected").text();
    var subjectName = $("#subjectNameSelectList option:selected").text();
    var locationName = $("#locationNameSelectList option:selected").text();

    var periodId = $("#PeriodId").val();
    var lessonId = $("#LessonId").val();
    var selectedDay = $("#schoolDaysSelectList option:selected").text();

    $.ajax({
        data: { teacherName: teacherName, subjectName: subjectName, locationName: locationName, periodId: periodId, lessonId: lessonId, selectedDay: selectedDay },
        url: "/TimeTable/SaveLesson/",
        method: "POST",
        success: function (data) {
            if (data.Success == null) {
                $("#div_LessonDetail_" + lessonId).html(data);
                $("#div_EditLesson").dialog("close");
                $("#" + lessonId).toggleClass("highlightSelectedLesson");
            } else {
                $("#div_showErrorMessages").html("");
                $(data.Response).each(function (index, message) {
                    $("#div_showErrorMessages").append("<p>" + message.ErrorMessage + "</p>");
                });
            }
        }
    });

});


var lessonId_global;

$(document).on("click", "#btnCancel", function () {
    $("#" + lessonId_global).removeClass("highlightSelectedLesson");
    $("#div_EditLesson").dialog("close");
});


$(document).on("change", "#schoolDaysSelectList", function () {
    var selectedDay = $("#schoolDaysSelectList option:selected").text();

    setHeadingTitle(selectedDay);

    $.ajax({
        url: "/TimeTable/TimeTableForTheDay/",
        data: { dayName: selectedDay },
        success: function (data) {
            $("#div_DaysTimeTable").html(data);
        }
    });

});

setCurrentDayInSchoolDayList();

function setCurrentDayInSchoolDayList() {
    var date = new Date();
    var weekdays = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var currentDayName = weekdays[date.getDay()];
    $("#schoolDaysSelectList option:contains(" + currentDayName + ")").attr('selected', 'selected');
    setHeadingTitle(currentDayName);
}


function setHeadingTitle(text) {
    $("#h1_heading").text("Weekly Time Table | " + text);
}

