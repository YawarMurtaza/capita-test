﻿using StructureMap;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using StructureMap.TypeRules;
using TimeTableManager.DataAccess;
using TimeTableManager.Mvc.Web.Infrastructure;
using TimeTableManager.Mvc.Web.ViewModels;
using TimeTableManager.ServiceManager;
using TimeTableManager.ServiceManager.Caching;
using TimeTableManager.Utilities;

namespace TimeTableManager.Mvc.Web
{
    public class MvcApplication : HttpApplication
    {
        /// <summary> Gets or sets the IoC Container for each http request. </summary>
        public IContainer Container
        {
            get
            {
                IContainer container = (IContainer)HttpContext.Current.Items["_Container"];
                return container;
            }
            set
            {
                HttpContext.Current.Items["_Container"] = value;
            }
        }

        public void Application_BeginRequest()
        {
            // Set the container as nested container from Structure map api each time a http request is received.
            this.Container = ObjectFactory.Container.GetNestedContainer();
        }


        public void Application_EndRequest()
        {
            // dispose the container when the http request has completed. Set the container to null. This will make the GC
            // to collect this object when next time it runs.
            this.Container.Dispose();
            this.Container = null;
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(typeof(UpdateLessonViewModel), new UpdateLessonViewModelBinder());

            log4net.Config.XmlConfigurator.Configure();

            // this fallback machanism is important. MVC is going to use this DependencyResolver 
            // for creating a lot of its core infrastructure and Nested Container wont be available when its making soe of those objects.
            // Thats because the HTTPContext or HttpContext.Current will be null at that time. Therefore, we need to provide 
            // ObjectFactory.Container to satisfy the need.
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(() => this.Container ?? ObjectFactory.Container));

            ObjectFactory.Configure(configureTypes =>
            {
                //cfg.AddRegistry(new MvcRegistry());

                //cfg.For<IValidatableObject>().Use<SaveLessonViewModel>();
                configureTypes.For<IApplicationLogger>().Use<ApplicationLogger>();

                // register caching...
                configureTypes.For<ICacheService>().Use<ApplicationCacheService>();

                // service managers....
                configureTypes.For<ITimeTableManager>().Use<ServiceManager.TimeTableManager>();

                configureTypes.For<ITeacherManager>().Use<TeacherManager>();
                configureTypes.For<ILocationManager>().Use<LocationManager>();
                configureTypes.For<ISubjectManager>().Use<SubjectManager>();
                configureTypes.For<ILessonManager>().Use<LessonManager>();


                // data access...
                configureTypes.For<ITimeTableDataAccess>().Use<TimeTableDataAccess>();

                // register the new isntance of structure map filter provider and pass in the nested container if its not null.
                // the filter provider intern builds the logFilter instance using Setter Injection.
                configureTypes.For<IFilterProvider>().Use(new StructureMapFilterProvider(() => this.Container ?? ObjectFactory.Container));

                configureTypes.SetAllProperties(prop => prop.Matching(matchingProp =>
                      matchingProp.DeclaringType.CanBeCastTo(typeof(ActionFilterAttribute))
                     && matchingProp.DeclaringType.Namespace.StartsWith("TimeTableManager.Mvc.Web.Filters")
                     && !matchingProp.PropertyType.IsPrimitive
                     && matchingProp.PropertyType != typeof(string)));

                configureTypes.Scan(s =>
                {
                    s.TheCallingAssembly();
                    s.WithDefaultConventions();
                    s.With(new ControllerConvention());

                });

            });

        }
    }
}
