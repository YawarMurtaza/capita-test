using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TimeTableManager.DomainModel;
using TimeTableManager.Mvc.Web.ExtensionMethods;
using TimeTableManager.Mvc.Web.Filters;
using TimeTableManager.Mvc.Web.ViewModels;
using TimeTableManager.ServiceManager;

namespace TimeTableManager.Mvc.Web.Controllers
{
    /// <summary> Defines the actions for Time Table related queries. </summary>
    public class TimeTableController : Controller
    {
        private readonly ITimeTableManager timeTableManager;
        
        public TimeTableController(ITimeTableManager ttManager)
        {   
            this.timeTableManager = ttManager;
        }
        
        public ActionResult Index()
        {
            SchoolWeekViewModel model = new SchoolWeekViewModel();
            return this.View(model);
        }
        
        public PartialViewResult TimeTableForTheDay(DayOfWeek dayName)
        {
            IEnumerable<Period> daysPeriods = this.timeTableManager.GetPeriods(dayName);
            IEnumerable<PeriodViewModel> periods = daysPeriods.ConvertToPeriodViewModel();
            return this.PartialView(periods);
        }
        
        [TeacherNamesProvider,  LocationNamesProvider, SubjectNamesProvider]
        public PartialViewResult EditLesson(EditLessonViewModel editViewModel)
        {
            LessonViewModel model = new LessonViewModel()
            {
                PeriodId = editViewModel.PeriodId,
                LessonId = editViewModel.LessonId
            };

            return this.PartialView("EditLesson", model);
        }
     
        [Log("UpdateLessonViewModel=[{model}]")]
        [HttpPost]
        public ActionResult SaveLesson(UpdateLessonViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                Lesson updatedLesson = this.timeTableManager.UpdateLesson(model.TeacherName, model.SubjectName, model.LocationName, model.PeriodId, model.LessonId, model.SelectedDay);
                return this.PartialView("DisplayTemplates/LessonDetailViewModel", updatedLesson.ConvertToLessonDetail(model.PeriodId));
                
            }
            else
            {
                return this.Json(new JsonResutlObjectViewModel  { Success = false, Response = this.ModelState.SelectMany(e => e.Value.Errors) });
            }
        }



    }
}