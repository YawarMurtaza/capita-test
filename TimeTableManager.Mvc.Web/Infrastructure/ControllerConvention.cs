using System;
using System.Web.Mvc;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;
using StructureMap.Pipeline;
using StructureMap.TypeRules;

namespace TimeTableManager.Mvc.Web.Infrastructure
{
    /// <summary> Defines the life cycle of a controller instance that is creatred by the StructureMap IoC Container.</summary>
    public class ControllerConvention : IRegistrationConvention
    {
        /// <summary> StructureMap will call this Process method for each type it encounters. </summary>
        /// <param name="type">Type to construct.</param>
        /// <param name="registry"></param>
        public void Process(Type type, Registry registry)
        {
            if (type.CanBeCastTo(typeof(Controller)) && !type.IsAbstract)
            {
                registry.For(type).LifecycleIs(new UniquePerRequestLifecycle());
            }
        }
    }
}