using System;
using System.Collections.Generic;
using System.Web.Mvc;
using StructureMap;

namespace TimeTableManager.Mvc.Web.Infrastructure
{
    /// <summary> Resolves the setter injection in the Filters. </summary>
    public class StructureMapFilterProvider : FilterAttributeFilterProvider
    {
        private readonly Func<IContainer> containerFactory;
        public StructureMapFilterProvider(Func<IContainer> containerFactory)
        {
            this.containerFactory = containerFactory;
        }

        /// <summary>
        /// Aggregates the filters from all of the filter providers into one collection.
        /// </summary>
        /// <returns>
        /// The collection filters from all of the filter providers.
        /// </returns>
        /// <param name="controllerContext">The controller context.</param><param name="actionDescriptor">The action descriptor.</param>
        public override IEnumerable<Filter> GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            IEnumerable<Filter> filters = base.GetFilters(controllerContext, actionDescriptor);

            IContainer container = this.containerFactory();

            foreach (Filter nextFilter in filters)
            {
                container.BuildUp(nextFilter.Instance);
                yield return nextFilter;
            }
        }
    }
}