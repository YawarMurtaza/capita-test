using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

using StructureMap;
using TimeTableManager.Mvc.Web.ExtensionMethods;
using TimeTableManager.Mvc.Web.ViewModels;
using TimeTableManager.ServiceManager;

namespace TimeTableManager.Mvc.Web.Infrastructure
{
    public class UpdateLessonViewModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            int periodId = int.Parse(request.Form.Get("PeriodId"));
            int lessonId = int.Parse(request.Form.Get("LessonId"));
            string teacherName = request.Form.Get("teacherName");
            string subjectName = request.Form.Get("subjectName");
            string locationName = request.Form.Get("locationName");
            DayOfWeek selectedDay = request.Form.Get("selectedDay").ConvertToDayOfWeekEnum();

            // get the IoC container from httpContext from controller context parameter.
            IContainer container = (IContainer)controllerContext.HttpContext.Items["_Container"];
            
            ITeacherManager teacherManager = container.GetInstance<ITeacherManager>();
            ILocationManager locManager = container.GetInstance<ILocationManager>();
            ILessonManager lessonManager = container.GetInstance<ILessonManager>();
            
            IValidatableObject  saveLessonViewModel = new UpdateLessonViewModel(teacherManager, locManager, lessonManager)
            {
                           PeriodId = periodId,
                           TeacherName = teacherName,
                           LessonId = lessonId,
                           SubjectName = subjectName,
                           SelectedDay = selectedDay,
                           LocationName = locationName
                    };

            var validationResults = new HashSet<ValidationResult>();

            var isValid = Validator.TryValidateObject(saveLessonViewModel, new ValidationContext(saveLessonViewModel, null, null), validationResults, true);

            if (!isValid)
            {
                foreach (var result in validationResults)
                {
                    bindingContext.ModelState.AddModelError("", result.ErrorMessage);
                }
            }

            return saveLessonViewModel;
        }
    }
}