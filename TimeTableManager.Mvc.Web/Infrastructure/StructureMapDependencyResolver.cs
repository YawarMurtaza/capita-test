﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StructureMap;

namespace TimeTableManager.Mvc.Web.Infrastructure
{
    public class StructureMapDependencyResolver : IDependencyResolver
    {
        private readonly Func<IContainer> containerFactory;

        public StructureMapDependencyResolver(Func<IContainer> containerFactory)
        {
            this.containerFactory = containerFactory;
        }

        /// <summary> Resolves singly registered services that support arbitrary object creation. </summary>
        /// <returns> The requested service or object. </returns>
        /// <param name="serviceType">The type of the requested service or object.</param>
        public object GetService(Type serviceType)
        {
            object obj = null;
            if (serviceType != null)
            {
                IContainer container = this.containerFactory();
                obj = serviceType.IsAbstract || serviceType.IsInterface ? // check if the serviceType is an abstract class or an interface.
                    container.TryGetInstance(serviceType) // TryGetInstance is for abstract classes and interfaces..
                    : container.GetInstance(serviceType); // GetInstance is for the concrete types...
            }

            return obj;
        }

        /// <summary> Resolves multiply registered services. </summary>
        /// <returns> The requested services. </returns>
        /// <param name="serviceType">The type of the requested services.</param>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            IEnumerable<object> allInstances = this.containerFactory().GetAllInstances(serviceType).Cast<object>();
            return allInstances;
        }
    }
}