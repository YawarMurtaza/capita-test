﻿using System.Web.Mvc;
using TimeTableManager.Mvc.Web.Filters;

namespace TimeTableManager.Mvc.Web.ViewModels
{
    public class LessonViewModel : IHaveLocationNameSelectList, IHaveSubjectNameSelectList, IHaveTeacherNameSelectList
    {
        public int PeriodId { get; set; }

        public int LessonId { get; set; }

        public SelectListItem[] AvailableLocationNames { get; set; }

        public SelectListItem[] AvailableSubjectNames { get; set; }

        public SelectListItem[] AvailableTeachersNames { get; set; }
    }
}