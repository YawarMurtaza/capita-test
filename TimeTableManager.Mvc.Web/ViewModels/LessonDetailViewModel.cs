﻿namespace TimeTableManager.Mvc.Web.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class LessonDetailViewModel
    {
        [Display(Name = "")]
        public int LessonId { get; set; }   

        [Display(Name = "")]
        public string SubjectName { get; set; }

        [Display(Name = "")]
        public string TeacherName { get; set; }

        [Display(Name = "")]
        public string Location { get; set; }

        [Display(Name = "")]
        public string ButtonText { get; set; }

        [Display(Name = "")]
        public int PeriodId { get; set; }

        [Display(Name = "")]
        public string ClassName { get; set; }
    }
}