
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TimeTableManager.DomainModel;
using TimeTableManager.Mvc.Web.ExtensionMethods;
using TimeTableManager.ServiceManager;


namespace TimeTableManager.Mvc.Web.ViewModels
{
    public class UpdateLessonViewModel : IValidatableObject
    {
        public int PeriodId { get; set; }
        public int LessonId { get; set; }
        public string TeacherName { get; set; }

        public string SubjectName { get; set; }

        public string LocationName { get; set; }

        public DayOfWeek SelectedDay { get; set; }

        private readonly ITeacherManager teacherManager;
        private readonly ILocationManager locationManager;
        private readonly ILessonManager lessonManager;

        public UpdateLessonViewModel(ITeacherManager manager, ILocationManager locationManager, ILessonManager lessonManager)
        {
            this.teacherManager = manager;
            this.locationManager = locationManager;
            this.lessonManager = lessonManager;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            bool isValidTeacherName = this.teacherManager.GetAllTeachers().Any(t => t.Name == this.TeacherName);
            if (!isValidTeacherName)
            {
                yield return new ValidationResult("Invalid Teacher Name.");
            }

            bool isValidLocation = this.locationManager.GetAllLocationNames().Any(l => l == this.LocationName);
            if (!isValidLocation)
            {
                yield return new ValidationResult("Invalid Room Name.");
            }

            IEnumerable<Lesson> lessons = this.lessonManager.GetAllLessonsForPeriodId(this.PeriodId, this.SelectedDay).ToList();

            bool validateTeacherForPeriod = lessons.Where(l => l.LessonId != this.LessonId).Select(l => l.Teacher).Any(t => t.Name == this.TeacherName);
            if (validateTeacherForPeriod)
            {
                yield return new ValidationResult("Teacher " + this.TeacherName + " is already assigned a period.");
            }

            bool validateLocationForPeriod = lessons.Where(l => l.LessonId != this.LessonId).Any(l => l.Location == this.LocationName);
            if (validateLocationForPeriod)
            {
                yield return new ValidationResult("Locaton " + this.LocationName+ " is already booked.");
            }
        }

        public override string ToString()
        {
            return string.Format("Location:{0}, LessonId:{1}, PeriodId:{2}, SelectedDay:{3}, SubjectName:{4},TeacherName:{5} ", this.LocationName, this.LessonId, this.PeriodId, this.SelectedDay, this.SubjectName, this.TeacherName);
        }
    }
}