using System;
using System.Collections.Generic;

namespace TimeTableManager.Mvc.Web.ViewModels
{
    public class EditLessonViewModel
    {
        public int PeriodId { get; set; }
        public int LessonId { get; set; }
        public DayOfWeek DayName { get; set; }
        public List<int> LessonIdsToDisable { get; set; }   

    }
}