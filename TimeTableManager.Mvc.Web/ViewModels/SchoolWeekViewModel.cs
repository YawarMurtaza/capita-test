namespace TimeTableManager.Mvc.Web.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    
    public class SchoolWeekViewModel
    {
        public SchoolDayViewModel SchoolDay { get; set; }

        [Display(Name = "Day"), DataType("WeekDays")]
        public string Day { get; set; }
    }
}