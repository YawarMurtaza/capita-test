﻿namespace TimeTableManager.Mvc.Web.ViewModels
{
    using System.Collections.Generic;

    public class SchoolDayViewModel
    {
        public string DayName { get; set; } 
        public IList<PeriodViewModel> Periods { get; set; }     
    }
}