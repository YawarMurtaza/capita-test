using System.Collections.Generic;

namespace TimeTableManager.Mvc.Web.ViewModels
{
    public class JsonResutlObjectViewModel
    {
        public bool Success { get; set; }
        public IEnumerable<System.Web.Mvc.ModelError> Response { get; set; }
    }
}