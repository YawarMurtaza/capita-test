﻿namespace TimeTableManager.Mvc.Web.ViewModels
{
    using System.Collections.Generic;

    public class PeriodViewModel
    {
        public int PeriodId  { get; set; }
        public IList<LessonDetailViewModel> Lessons { get; set; }    
    }
}