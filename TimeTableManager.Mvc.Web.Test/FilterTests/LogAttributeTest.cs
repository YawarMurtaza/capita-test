﻿using System.Collections.Generic;
using System.Reflection.Emit;
using System.Web.Mvc;
using System.Web.Mvc.Async;
using System.Web.UI.WebControls.Expressions;
using NUnit.Framework;
using Rhino.Mocks;
using TimeTableManager.Mvc.Web.Controllers;
using TimeTableManager.Mvc.Web.Filters;
using TimeTableManager.Utilities;

namespace TimeTableManager.Mvc.Web.Test.FilterTests
{
    [TestFixture]
    public class LogAttributeTest
    {
        private IApplicationLogger mockLogger;

        [SetUp]
        public void Setup()
        {
            this.mockLogger = MockRepository.GenerateMock<IApplicationLogger>();
        }

        [TearDown]
        public void TearDown()
        {
            this.mockLogger = null;
        }

        
        [Test]
        public void OnActionExecuted_ShouldLogSuccessfully()
        {
            //
            // Arrange.
            //
            this.mockLogger.Stub(logger => logger.LogInfo(Arg<string>.Is.Anything, Arg<object[]>.Is.Anything));

            ActionExecutingContext mockExecutingContext = this.GetMockExecutingContext();
            ActionExecutedContext mockExecutedContext = this.GetMockExecutedContext();

            LogAttribute attribute = new LogAttribute("This is a mock description {ID}");
            attribute.FileLogger = this.mockLogger;
            attribute.OnActionExecuting(mockExecutingContext);
            //
            // Act.
            //
            
            attribute.OnActionExecuted(mockExecutedContext);

            this.mockLogger.AssertWasCalled(logger => logger.LogInfo(Arg<string>.Is.Anything, Arg<object[]>.Is.Anything));


        }


        private ActionExecutingContext GetMockExecutingContext()
        {
            ActionExecutingContext context = new ActionExecutingContext();
            IDictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("ID", 123);
            context.ActionParameters = parameters;
            return context;
        }

        private ActionExecutedContext GetMockExecutedContext()
        {
            ActionExecutedContext context = MockRepository.GenerateStub<ActionExecutedContext>();
            ActionDescriptor mockDescriptor = MockRepository.GenerateMock<ActionDescriptor>();
            mockDescriptor.Stub(ad => ad.ActionName).Return("Test_Method_Just_For_Fun");
            
            ControllerDescriptor contDescriptor = MockRepository.GenerateMock<ControllerDescriptor>();
            contDescriptor.Stub(cd => cd.ControllerName).Return("Test_Controller_Just_For_Fun");

            mockDescriptor.Stub(desc => desc.ControllerDescriptor).Return(contDescriptor);

            context.ActionDescriptor = mockDescriptor;
            return context;
        }
    }
}