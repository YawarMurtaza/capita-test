﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NUnit.Framework;
using Rhino.Mocks;
using TimeTableManager.DomainModel;
using TimeTableManager.Mvc.Web.Filters;
using TimeTableManager.Mvc.Web.ViewModels;
using TimeTableManager.ServiceManager;


namespace TimeTableManager.Mvc.Web.Test.FilterTests
{
    [TestFixture]
    public class LocationNamesProviderAttributeTest
    {
        private ILocationManager mockLocManager;
        private ILessonManager mockLessonManager;

        [SetUp]
        public void Setup()
        {
            this.mockLessonManager = MockRepository.GenerateMock<ILessonManager>();
            this.mockLocManager = MockRepository.GenerateMock<ILocationManager>();
        }

        [TearDown]
        public void TearDown()
        {
            this.mockLessonManager = null;
            this.mockLocManager = null;
        }

        [Test]
        public void OnActionExecutedShouldSetTheLocationSelectItemListWithSomeDisabledLocations()
        {
            //
            // Arrange.
            //
            int periodId = 4;
            DayOfWeek dayName =  DayOfWeek.Friday;

            IEnumerable<Lesson> mockLessons = this.GetMockLessons();
            IEnumerable<string> mockLocations = this.GetMockLocations();
            ActionExecutingContext mockExecutingContext = this.GetMockActionExecutingContext(periodId, dayName);
            ActionExecutedContext mockExecutedContext = this.GetMockExecutedContext();

            this.mockLessonManager.Stub(mngr => mngr.GetAllLessonsForPeriodId(periodId, dayName)).Return(mockLessons);
            this.mockLocManager.Stub(mngr => mngr.GetAllLocationNames()).Return(mockLocations);

            LocationNamesProviderAttribute attribute = new LocationNamesProviderAttribute();
            attribute.LessonManager = this.mockLessonManager;
            attribute.LocationManager = this.mockLocManager;

            attribute.OnActionExecuting(mockExecutingContext);

            //
            // Act.
            //
            attribute.OnActionExecuted(mockExecutedContext);


            //
            // Assert.
            //

            PartialViewResult result = mockExecutedContext.Result as PartialViewResult;
            LessonViewModel lessonViewModel = result.Model as LessonViewModel;

            Assert.That(lessonViewModel.AvailableLocationNames[1].Disabled, Is.True);
            Assert.That(lessonViewModel.AvailableLocationNames[3].Disabled, Is.True);

            Assert.That(lessonViewModel.AvailableLocationNames[0].Disabled, Is.False);
            Assert.That(lessonViewModel.AvailableLocationNames[2].Disabled, Is.False);

        }

        private ActionExecutedContext GetMockExecutedContext()
        {
            LessonViewModel mockLessonViewModel = new LessonViewModel();

            ViewDataDictionary mockDataDic = new ViewDataDictionary();
            mockDataDic.Model = mockLessonViewModel;

            PartialViewResult presult = MockRepository.GenerateStub<PartialViewResult>();
            presult.ViewData = mockDataDic;

            ActionExecutedContext context = new ActionExecutedContext();
            context.Result = presult;


            return context;
        }

        private ActionExecutingContext GetMockActionExecutingContext(int periodId, DayOfWeek dayName)
        {
            EditLessonViewModel mockModel = new EditLessonViewModel()
            {
                DayName = dayName,
                PeriodId = periodId,
                LessonIdsToDisable = new List<int>() { 27, 29 },
                LessonId = 26
            };

            var parameters = new Dictionary<string, object>();
            parameters.Add("editViewModel", mockModel);
            ActionExecutingContext context = new ActionExecutingContext();
            context.ActionParameters = parameters;
            return context;
        }

        private IEnumerable<string> GetMockLocations()
        {
            return this.GetMockLessons().Select(l => l.Location);
        }

        private IEnumerable<Lesson> GetMockLessons()
        {
            return new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 26,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 27,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 28,
                                SubjectName = "Mathematics",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 29,
                                SubjectName = "Physics",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 30,
                                SubjectName = "Chemistry",
                                Location = "Room # 404",
                                Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                            },
                        };
        }

    }
}
