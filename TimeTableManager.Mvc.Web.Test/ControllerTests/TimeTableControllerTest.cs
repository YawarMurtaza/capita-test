﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NUnit.Framework;
using Rhino.Mocks;
using TimeTableManager.DomainModel;
using TimeTableManager.Mvc.Web.Controllers;
using TimeTableManager.Mvc.Web.ViewModels;
using TimeTableManager.ServiceManager;

namespace TimeTableManager.Mvc.Web.Test.ControllerTests
{
    [TestFixture]
    public class TimeTableControllerTest
    {
        private ITimeTableManager mockManager;
        private TimeTableController controller;
        [SetUp]
        public void Setup()
        {
            this.mockManager = MockRepository.GenerateMock<ITimeTableManager>();
            this.controller = new TimeTableController(this.mockManager);
        }

        [TearDown]
        public void TearDown()
        {
            this.mockManager = null;
            this.controller = null;
        }


        [Test]
        public void Inxex_should_return_SchoolWeekViewModel()
        {
            //
            // Arrange.
            //

            this.controller = new TimeTableController(this.mockManager);

            //
            // Act.
            //
            ActionResult actionResult = controller.Index();

            //
            // Assert.
            //

            ViewResult vResult = (ViewResult)actionResult;
            SchoolWeekViewModel model = vResult.Model as SchoolWeekViewModel;
            Assert.That(model, !Is.Null);
        }

        [Test]
        public void TimeTableForTheDay_ReturnsPartialViewWithPeriodViewModels()
        {
            //
            // Arrange.
            //
            IEnumerable<Period> mockPeriods = this.GetMockPeriods();
            DayOfWeek dayName = DayOfWeek.Friday;
            this.mockManager.Stub(mngr => mngr.GetPeriods(dayName)).Return(mockPeriods);

            //
            // Act.
            //
            PartialViewResult partialView = this.controller.TimeTableForTheDay(dayName);

            //
            // Assert.
            //

            IEnumerable<PeriodViewModel> model = partialView.Model as IEnumerable<PeriodViewModel>;
            Assert.That(model, !Is.Null);
            Assert.That(model.ToList()[0].Lessons[0].Location, Is.EqualTo("Room # 110"));
            Assert.That(model.ToList()[0].Lessons[0].SubjectName, Is.EqualTo("Physics"));
            Assert.That(model.ToList()[0].Lessons[0].TeacherName, Is.EqualTo("Charlie C"));

        }

        [Test, TestCaseSource("GetUpdateLessonViewModels")]
        public void SaveLesson_ShouldReturnUpdateLessonViewModelsWithUpdatedLessonsSuccessfully(UpdateLessonViewModel mockModel)
        {
            //
            // Arrange.
            //
            
            Lesson mockLesson = this.GetMockLesson();
            this.mockManager.Stub(mngr => mngr.UpdateLesson
                (mockModel.TeacherName, mockModel.SubjectName, mockModel.LocationName, mockModel.PeriodId, mockModel.LessonId, mockModel.SelectedDay))
                .Return(mockLesson);

            //
            // Act.
            //
            ActionResult actionResult = this.controller.SaveLesson(mockModel);

            //
            // Assert.
            //
            PartialViewResult partialView = actionResult as PartialViewResult;
            Assert.That(partialView, !Is.Null);

            LessonDetailViewModel model = partialView.Model as LessonDetailViewModel;
            Assert.That(model.Location.StartsWith("Room #"), Is.True);
            Assert.That(model.LessonId, Is.GreaterThan(0));
            Assert.That(string.IsNullOrEmpty(model.SubjectName), Is.False);
            Assert.That(string.IsNullOrEmpty(model.TeacherName), Is.False);
        }


        [Test, TestCaseSource("MockUpdateLessonViewModel")]
        public void SaveLesson_ShouldReturnFailureAndErrorMessagesInJsonObject(UpdateLessonViewModel mockModel)
        {
            //
            // Arrange.
            //

            this.controller.ModelState.Add(new KeyValuePair<string, ModelState>("Error", new ModelState() { Errors = { "TeacherName is invalid!"}}));

            //
            // Act.
            //

            ActionResult actionResult = this.controller.SaveLesson(model: mockModel);

            //
            // Assert.
            //

            JsonResult jsonResult = actionResult as JsonResult;

            JsonResutlObjectViewModel data = jsonResult.Data as JsonResutlObjectViewModel;

            Assert.That(data.Success, Is.False);

            foreach (ModelError nextError in data.Response)
            {
                Assert.That(nextError.ErrorMessage, Is.EqualTo("TeacherName is invalid!"));
            }

        }

       
        private static IEnumerable<UpdateLessonViewModel> MockUpdateLessonViewModel()
        {
            ITeacherManager mockTeacherManager = MockRepository.GenerateMock<ITeacherManager>();
            ILocationManager mockLocManager = MockRepository.GenerateMock<ILocationManager>();
            ILessonManager mockLessManager = MockRepository.GenerateMock<ILessonManager>();

            yield return new UpdateLessonViewModel(mockTeacherManager, mockLocManager, mockLessManager)
            {
                LessonId = 1,
                LocationName = "Room # 101",
                PeriodId = 1,
                SelectedDay =  DayOfWeek.Thursday,
                SubjectName = "Physics",
                TeacherName = "Albert Einstein"
            };
        }

        private Lesson GetMockLesson()
        {
            return new Lesson()
            {
                LessonId = 1,
                SubjectName = "Physics",
                Location = "Room # 101",
                Teacher = new Teacher() { Name = "Albert E" }
            };
        }
        

        private static IEnumerable<UpdateLessonViewModel> GetUpdateLessonViewModels()
        {

            ITeacherManager mockTeacherManager = MockRepository.GenerateMock<ITeacherManager>();
            ILocationManager mockLocManager = MockRepository.GenerateMock<ILocationManager>();
            ILessonManager mockLessManager = MockRepository.GenerateMock<ILessonManager>();

            yield return new UpdateLessonViewModel(mockTeacherManager, mockLocManager, mockLessManager)
            {
                LessonId = 1,LocationName = "Room # 101",PeriodId = 1,SelectedDay = DayOfWeek.Thursday, SubjectName = "Physics",TeacherName = "Albert Einstein"
            };

            yield return new UpdateLessonViewModel(mockTeacherManager, mockLocManager, mockLessManager)
            {
                LessonId = 15, LocationName = "Room # 505", PeriodId = 3, SelectedDay = DayOfWeek.Monday, SubjectName = "Chemistry", TeacherName = "Albert Einstein"
            };

            yield return new UpdateLessonViewModel(mockTeacherManager, mockLocManager, mockLessManager)
            {
                LessonId = 30, LocationName = "Room # 303", PeriodId = 6,  SelectedDay = DayOfWeek.Wednesday, SubjectName = "Computer Science", TeacherName = "Albert Einstein"
            };
        }

        private IEnumerable<Period> GetMockPeriods()
        {
            return new List<Period>()
            
            
                {
                    new Period()
                    {
                        PeriodId = 1,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 1,
                                SubjectName = "Physics",
                                Location = "Room # 110",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 2,
                                SubjectName = "Chemistry",
                                Location = "Room # 404",
                                Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 3,
                                SubjectName = "Mathematics",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 4,
                                SubjectName = "Computing",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Scott Hanselman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 5,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 2,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 6,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 7,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 8,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 9,
                                SubjectName = "Music",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "A R Rehman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 10,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 3,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 11,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 12,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 13,
                                SubjectName = "Mathematics",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 14,
                                SubjectName = "Computing",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Scott Hanselman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 15,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 4,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 16,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 17,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 18,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 19,
                                SubjectName = "Music",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "A R Rehman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 20,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 5,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 21,
                                SubjectName = "Computer Programing",
                                Location = "Room # 110",
                                Teacher = new Teacher() { Name = "Robert Lafore", Id = 501}
                            },
                            new Lesson()
                            {
                                LessonId = 22,
                                SubjectName = "Circuit Analysis",
                                Location = "Room # 220",
                                Teacher = new Teacher() { Name = "William Hart", Id = 502}
                            },
                            new Lesson()
                            {
                                LessonId = 23,
                                SubjectName = "Digital Logic",
                                Location = "Room # 330",
                                Teacher = new Teacher() { Name = "Moris Mano", Id = 503}
                            },
                            new Lesson()
                            {
                                LessonId = 24,
                                SubjectName = "Reletional Database",
                                Location = "Room # 440",
                                Teacher = new Teacher() { Name = "Itzik Ben-Gan", Id = 504}
                            },
                            new Lesson()
                            {
                                LessonId = 25,
                                SubjectName = "Advanced Mathematics",
                                Location = "Room # 550",
                                Teacher = new Teacher() { Name = "Kreyszig", Id = 505}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 6,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 26,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 27,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 28,
                                SubjectName = "Mathematics",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 29,
                                SubjectName = "Physics",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 30,
                                SubjectName = "Chemistry",
                                Location = "Room # 404",
                                Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                            },
                        }
                    }
        };
        }
    }
}