using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap;
using TimeTableManager.DomainModel;
using TimeTableManager.Mvc.Web.Infrastructure;
using TimeTableManager.Mvc.Web.ViewModels;
using TimeTableManager.ServiceManager;

namespace TimeTableManager.Mvc.Web.Test.InfrastructureTests
{
    [TestFixture]
    public class UpdateLessonViewModelBinderTest
    {
        [Test, TestCase(1, 1, "Winston Churchill", "Physics", "Room # 786", DayOfWeek.Friday)]
        public void BindModel_ShouldReturnIValidatableObjectObjectSuccessfully(int periodId, int lessonId, string teacherName, string subjectName, string locationName, DayOfWeek selectedDay)
        {
            //
            // Arrange.
            //
            ControllerContext mockContext = this.GetMockControllerContext(periodId, lessonId, teacherName, subjectName, locationName, selectedDay);
            UpdateLessonViewModelBinder modelBinder = new UpdateLessonViewModelBinder();

            //
            // Act.
            //

            UpdateLessonViewModel updateLessonModel = modelBinder.BindModel(mockContext, null) as UpdateLessonViewModel;

            //
            // Arrange.
            //

            Assert.That(updateLessonModel.LessonId, Is.EqualTo(lessonId));
            Assert.That(updateLessonModel.PeriodId, Is.EqualTo(periodId));
            Assert.That(updateLessonModel.SelectedDay, Is.EqualTo(selectedDay));
            Assert.That(updateLessonModel.SubjectName, Is.EqualTo(subjectName));
            Assert.That(updateLessonModel.TeacherName, Is.EqualTo(teacherName));
            Assert.That(updateLessonModel.LocationName, Is.EqualTo(locationName));


        }

        private ControllerContext GetMockControllerContext(int periodId, int lessonId, string teacherName, string subjectName, string locationName, DayOfWeek selectedDay)
        {
            HttpContextBase mockHttpBaseContext = MockRepository.GenerateMock<HttpContextBase>();
            Dictionary<string, object> mockItems = new Dictionary<string, object>();

            ITeacherManager mockTeacherMangager = MockRepository.GenerateMock<ITeacherManager>();
            IEnumerable<Teacher> mockTeachers = this.GetMockTeachers();
            mockTeacherMangager.Stub(manager => manager.GetAllTeachers()).Return(mockTeachers);


            ILocationManager mockLocationManager = MockRepository.GenerateMock<ILocationManager>();
            IEnumerable<string> mockLocaitonNames = this.GetMockLocationNames();
            mockLocationManager.Stub(manager => manager.GetAllLocationNames()).Return(mockLocaitonNames);

            ILessonManager mockLessManager = MockRepository.GenerateMock<ILessonManager>();
            IEnumerable<Lesson> mockPeriods = this.GetMockLessons();
            mockLessManager.Stub(manager => manager.GetAllLessonsForPeriodId(periodId, selectedDay)).Return(mockPeriods);

            IContainer container = ObjectFactory.Container.GetNestedContainer();
            container.Configure(configureTypes =>
            {
                configureTypes.For<ITeacherManager>().AddInstances(item =>
                {
                    item.IsThis(mockTeacherMangager);
                });

                configureTypes.For<ILocationManager>().AddInstances(item =>
                {
                    item.IsThis(mockLocationManager);
                });


                configureTypes.For<ILessonManager>().AddInstances(item =>
                {
                    item.IsThis(mockLessManager);
                });

            });




            mockItems.Add("_Container", container);

            mockHttpBaseContext.Stub(ctx => ctx.Items).Return(mockItems);
            HttpRequestBase mockRequest = MockRepository.GenerateMock<HttpRequestBase>();

            NameValueCollection mockFormItemCollection = new NameValueCollection();

            mockFormItemCollection.Add("PeriodId", periodId.ToString());
            mockFormItemCollection.Add("LessonId", lessonId.ToString());
            mockFormItemCollection.Add("teacherName", teacherName);
            mockFormItemCollection.Add("subjectName", subjectName);
            mockFormItemCollection.Add("locationName", locationName);
            mockFormItemCollection.Add("selectedDay", selectedDay.ToString());

            mockRequest.Stub(request => request.Form).Return(mockFormItemCollection);

            mockHttpBaseContext.Stub(ctx => ctx.Request).Return(mockRequest);

            ControllerContext context = MockRepository.GenerateMock<ControllerContext>();

            context.Stub(ctx => ctx.HttpContext).Return(mockHttpBaseContext);


            return context;
        }

        private IEnumerable<Lesson> GetMockLessons()
        {
            return new List<Lesson>()
            {

                new Lesson()
                {
                    LessonId = 1,
                    SubjectName = "Physics",
                    Location = "Room # 786",
                    Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                },
                new Lesson()
                {LessonId = 2,
                    SubjectName = "Chemistry",
                    Location = "Room # 404",
                    Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                },
                new Lesson()
                {
                    LessonId = 3,
                    SubjectName = "Math",
                    Location = "Room # 505",
                    Teacher = new Teacher() { Name = "Jonathan R", Id = 3}
                },
                new Lesson()
                {
                    LessonId = 4,
                    SubjectName = "Computing",
                    Location = "Room # 303",
                    Teacher = new Teacher() { Name = "Scott Hanselman", Id = 4}
                },
                new Lesson()
                {
                    LessonId = 5,
                    SubjectName = "Physical Education",
                    Location = "Room # 606",
                    Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                }


            };
        }

        private IEnumerable<string> GetMockLocationNames()
        {
            return new List<string>()
            {
                "Room # 786"
            };
        }

        private IEnumerable<Teacher> GetMockTeachers()
        {
            return new List<Teacher>()
            {
                new Teacher() { Id = 123, Name = "Sir Robert Walpole" },
                new Teacher() { Id = 122, Name = "Henry Pelham" },
                new Teacher() { Id = 124, Name = "Margaret Thatcher" },
                new Teacher() { Id = 125, Name = "Tony Blair" },
                new Teacher() { Id = 125, Name = "Winston Churchill" },
            };
        }
    }
}