﻿using System;
using System.Collections.Generic;

namespace TimeTableManager.DomainModel
{
    public class SchoolDay
    {
        /// <summary> Gets or sets the name of the day. </summary>
        public DayOfWeek DayName { get; set; } 
        public IList<Period> Periods { get; set; }  
    }
}