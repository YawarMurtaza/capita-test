﻿using System.Collections.Generic;

namespace TimeTableManager.DomainModel
{
    public class Period
    {
        public int PeriodId { get; set; } 
        public IList<Lesson> Lessons { get; set; }    
    }
}