﻿using System.Collections.Generic;

namespace TimeTableManager.DomainModel
{
    public class Lesson
    {
        public int LessonId { get; set; }  
        public int MaxStudents { get; set; }
        public string SubjectName { get; set; }
        public string Location { get; set; }

        public Teacher Teacher { get; set; }
        public IList<FormGroup> FormGroups { get; set; }    
    }
}