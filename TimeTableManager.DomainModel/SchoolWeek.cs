﻿using System.Collections.Generic;

namespace TimeTableManager.DomainModel
{
    public class SchoolWeek
    {
        public IList<SchoolDay> SchoolDays { get; set; }    
    }
}