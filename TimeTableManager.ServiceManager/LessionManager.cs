﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimeTableManager.DomainModel;
namespace TimeTableManager.ServiceManager
{
    public class LessonManager : ILessonManager
    {
        private readonly ITimeTableManager ttManager;

        public LessonManager(ITimeTableManager ttManager)
        {
            this.ttManager = ttManager;
        }

        public IEnumerable<Lesson> GetAllLessonsForPeriodId(int periodId, DayOfWeek dayName)
        {
            IEnumerable<Lesson> lessons = this.ttManager.GetPeriods(dayName).First(p => p.PeriodId == periodId).Lessons;
            return lessons;
        }
    }





}
