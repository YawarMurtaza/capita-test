﻿using System.Collections.Generic;
using System.Linq;
using TimeTableManager.ServiceManager.Caching;

namespace TimeTableManager.ServiceManager
{
    public interface ISubjectManager
    {
        IEnumerable<string> GetAllSubjectNames();
    }

    public class SubjectManager : ISubjectManager
    {
        private readonly ITimeTableManager ttManager;

        private readonly ICacheService cache;

        public SubjectManager(ITimeTableManager ttManager, ICacheService cache)
        {
            this.ttManager = ttManager;
            this.cache = cache;
        }

        public IEnumerable<string> GetAllSubjectNames()
        {
            IEnumerable<string> allSubjects = this.cache.GetOrSet("AllSubjects", () => this.ttManager.GetAllPeriods().SelectMany(p => p.Lessons.Select(l => l.SubjectName)));
            return allSubjects.Distinct().OrderBy(s => s);
        }
    }
}