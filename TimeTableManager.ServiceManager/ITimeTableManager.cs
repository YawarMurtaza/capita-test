using System;
using System.Collections.Generic;
using TimeTableManager.DomainModel;

namespace TimeTableManager.ServiceManager
{
    public interface ITimeTableManager
    {
        IEnumerable<Period> GetAllPeriods();
        IEnumerable<Period> GetPeriods(DayOfWeek dayName);
        Lesson UpdateLesson(string teacherName, string subjectName, string locationName, int periodId, int lessonId, DayOfWeek dayName);
    }
}