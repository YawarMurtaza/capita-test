using System.Collections.Generic;

namespace TimeTableManager.ServiceManager
{
    public interface ILocationManager
    {
        IEnumerable<string> GetAllLocationNames();
    }
}