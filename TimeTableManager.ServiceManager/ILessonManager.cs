﻿using System;
using System.Collections.Generic;
using TimeTableManager.DomainModel;

namespace TimeTableManager.ServiceManager
{
    public interface ILessonManager
    {
        IEnumerable<Lesson> GetAllLessonsForPeriodId(int periodId, DayOfWeek dayName);
    }
}