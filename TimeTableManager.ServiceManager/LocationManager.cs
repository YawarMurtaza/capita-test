﻿using System.Collections.Generic;
using System.Linq;
using TimeTableManager.ServiceManager.Caching;

namespace TimeTableManager.ServiceManager
{
    public class LocationManager : ILocationManager
    {
        private readonly ITimeTableManager ttManager;

        private readonly ICacheService cache;

        public LocationManager(ITimeTableManager ttManager, ICacheService cache)
        {
            this.ttManager = ttManager;
            this.cache = cache;
        }

        public IEnumerable<string> GetAllLocationNames()
        {
            IEnumerable<string> allRooms = this.cache.GetOrSet("AllRooms", () => this.ttManager.GetAllPeriods().SelectMany(p => p.Lessons.Select(l => l.Location)));
            allRooms = allRooms.Distinct();
            return allRooms;
        }
    }
}