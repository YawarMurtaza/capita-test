using System;
using System.Collections.Generic;
using System.Linq;
using TimeTableManager.DataAccess;
using TimeTableManager.DomainModel;
using TimeTableManager.ServiceManager.Caching;

namespace TimeTableManager.ServiceManager
{
    public class TimeTableManager : ITimeTableManager
    {
        private readonly ITimeTableDataAccess timeTableDataAccess;
        
        public TimeTableManager(ITimeTableDataAccess timeTableDataAccess, ICacheService cache)
        {
            this.timeTableDataAccess = timeTableDataAccess;
        }
      
        public Lesson UpdateLesson(string teacherName, string subjectName, string locationName, int periodId, int lessonId, DayOfWeek dayName)
        {
            Lesson lesson = this.timeTableDataAccess.UpdateLesson(teacherName, subjectName, locationName, periodId, lessonId, dayName);
            return lesson;
        }

        public IEnumerable<Period> GetAllPeriods()
        {
            return this.timeTableDataAccess.GetAllSchoolDays().SelectMany(sd => sd.Periods);
        }

        public IEnumerable<Period> GetPeriods(DayOfWeek dayName)
        {
            SchoolDay schoolDay = this.timeTableDataAccess.GetSchoolDayFor(dayName);
            return schoolDay.Periods;
        }
    }
}

