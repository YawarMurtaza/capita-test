﻿using System.Collections.Generic;
using TimeTableManager.DomainModel;

namespace TimeTableManager.ServiceManager
{
    public interface ITeacherManager
    {
        IEnumerable<Teacher> GetAllTeachers();
    }
}