using System.Collections.Generic;
using System.Linq;
using TimeTableManager.DomainModel;

namespace TimeTableManager.ServiceManager
{
    public class TeacherManager : ITeacherManager
    {
        private readonly ITimeTableManager ttManager;
        
        public TeacherManager(ITimeTableManager ttManager)
        {
            this.ttManager = ttManager;
        }

        public IEnumerable<Teacher> GetAllTeachers()
        {
            IEnumerable<Teacher> teachers = this.ttManager.GetAllPeriods().SelectMany(p => p.Lessons.Select(l => l.Teacher));
            teachers = teachers.GroupBy(t => t.Name).Select(g => g.First()).OrderBy(teacher => teacher.Name);
            return teachers;
        }
    }
}