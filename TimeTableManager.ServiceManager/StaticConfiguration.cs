using System;
using System.Configuration;

namespace TimeTableManager.ServiceManager
{
    public static class StaticConfiguration
    {
        /// <summary>
        /// Gets the minutes to clear the memory cache object.
        /// </summary>
        /// <value>
        /// The minutes to clear the memory cache object.
        /// </value>
        public static int MinutesToClearTheMemoryCacheObject
        {
            get
            {
                // default caching time 1hr.
                int minutesToClearTheMemoryCacheObject = 30;
                if (ConfigurationManager.AppSettings["MinutesToClearTheMemoryCacheObject"] != null)
                {
                    minutesToClearTheMemoryCacheObject = Convert.ToInt32(ConfigurationManager.AppSettings["MinutesToClearTheMemoryCacheObject"]);
                }

                return minutesToClearTheMemoryCacheObject;
            }
        }
    }
}