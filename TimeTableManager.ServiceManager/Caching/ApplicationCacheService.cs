using System;
using System.Runtime.Caching;

namespace TimeTableManager.ServiceManager.Caching
{
    /// <summary>
    /// The letting cache service.
    /// </summary>
    public class ApplicationCacheService : ICacheService
    {
        /// <summary> Retrieves the cached item from Memory cache, if the item is not already cashed, it will call the delegate and cashe the result. </summary>
        /// <typeparam name="T">Type of cached item.</typeparam>
        /// <param name="cacheKey">Key of cached item.</param>
        /// <param name="getItemFromSourceCallback">A call back delegate if item is not already in the cache.</param>
        /// <returns>Cached item.</returns>
        public T GetOrSet<T>(string cacheKey, Func<T> getItemFromSourceCallback) where T : class
        {
            // try to retreive the cach item from the memory...
            T item = MemoryCache.Default.Get(cacheKey) as T;

            if (item == null)
            {
                // if the item is not available in the memory, then call the delegate.
                item = getItemFromSourceCallback();

                // and store the result back into this memory with the same item key value.
                MemoryCache.Default.Add(cacheKey, item, DateTime.Now.AddMinutes(StaticConfiguration.MinutesToClearTheMemoryCacheObject)); // time from appSettings.
            }

            return item;
        }
    }
}