using System;
using System.Collections.Generic;
using System.Linq;
using TimeTableManager.DomainModel;
namespace TimeTableManager.DataAccess
{
    public static class TimeTableRepository
    {
        #region MONDAY LIST
        private static SchoolDay monday = new SchoolDay()
        {
            DayName = DayOfWeek.Monday,  
            Periods = new List<Period>()
                {
                    new Period()
                    {
                        PeriodId = 1,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 1,
                                SubjectName = "",
                                Location = "",
                                Teacher = new Teacher() { Name = "", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 2,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 3,
                                SubjectName = "Math",
                                Location = "Room # 505",
                                Teacher = new Teacher() { Name = "Jonathan R", Id = 3}
                            },
                            new Lesson()
                            {
                                LessonId = 4,
                                SubjectName = "Art & Design",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Pablo Picasso", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 5,
                                SubjectName = "Biology",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Henry Gray", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 2,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 6,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 7,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 8,
                                SubjectName = "",
                                Location = "",
                                Teacher = new Teacher() { Name = "", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 9,
                                SubjectName = "Music",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "A R Rehman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 10,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 3,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 11,
                                SubjectName = "Physics",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 12,
                                SubjectName = "Chemistry",
                                Location = "Room # 404",
                                Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 13,
                                SubjectName = "Math",
                                Location = "Room # 505",
                                Teacher = new Teacher() { Name = "Jonathan R", Id = 3}
                            },
                            new Lesson()
                            {
                                LessonId = 14,
                                SubjectName = "Art & Design",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Pablo Picasso", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 15,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 4,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 16,
                                SubjectName = "Physics",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 17,
                                SubjectName = "Chemistry",
                                Location = "Room # 404",
                                Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 18,
                                SubjectName = "Math",
                                Location = "Room # 505",
                                Teacher = new Teacher() { Name = "Jonathan R", Id = 3}
                            },
                            new Lesson()
                            {
                                LessonId = 19,
                                SubjectName = "Art & Design",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Pablo Picasso", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 20,
                                SubjectName = "Biology",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Henry Gray", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 5,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 21,
                                SubjectName = "Physics",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 22,
                                SubjectName = "Chemistry",
                                Location = "Room # 404",
                                Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 23,
                                SubjectName = "Math",
                                Location = "Room # 505",
                                Teacher = new Teacher() { Name = "Jonathan R", Id = 3}
                            },
                            new Lesson()
                            {
                                LessonId = 24,
                                SubjectName = "Art & Design",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Pablo Picasso", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 25,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 6,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 26,
                                SubjectName = "Computer Programing",
                                Location = "Room # 110",
                                Teacher = new Teacher() { Name = "Robert Lafore", Id = 501}
                            },
                            new Lesson()
                            {
                                LessonId = 27,
                                SubjectName = "Circuit Analysis",
                                Location = "Room # 220",
                                Teacher = new Teacher() { Name = "William Hart", Id = 502}
                            },
                            new Lesson()
                            {
                                LessonId = 28,
                                SubjectName = "Digital Logic",
                                Location = "Room # 330",
                                Teacher = new Teacher() { Name = "Moris Mano", Id = 503}
                            },
                            new Lesson()
                            {
                                LessonId = 29,
                                SubjectName = "Reletional Database",
                                Location = "Room # 440",
                                Teacher = new Teacher() { Name = "Itzik Ben-Gan", Id = 504}
                            },
                            new Lesson()
                            {
                                LessonId = 30,
                                SubjectName = "Advanced Mathematics",
                                Location = "Room # 550",
                                Teacher = new Teacher() { Name = "Kreyszig", Id = 505}
                            }
                        }
                    }
                }
        };
        #endregion MONDAY LIST

        #region TUESDAY LIST
        private static SchoolDay tuesday = new SchoolDay()
        {
            DayName = DayOfWeek.Tuesday,
            Periods = new List<Period>()
                {
                    new Period()
                    {
                        PeriodId = 1,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 1,
                                SubjectName = "Computer Programing",
                                Location = "Room # 110",
                                Teacher = new Teacher() { Name = "Robert Lafore", Id = 501}
                            },
                            new Lesson()
                            {
                                LessonId = 2,
                                SubjectName = "Circuit Analysis",
                                Location = "Room # 220",
                                Teacher = new Teacher() { Name = "William Hart", Id = 502}
                            },
                            new Lesson()
                            {
                                LessonId = 3,
                                SubjectName = "Digital Logic",
                                Location = "Room # 330",
                                Teacher = new Teacher() { Name = "Moris Mano", Id = 503}
                            },
                            new Lesson()
                            {
                                LessonId = 4,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 504}
                            },
                            new Lesson()
                            {
                                LessonId = 5,
                                SubjectName = "Advanced Mathematics",
                                Location = "Room # 550",
                                Teacher = new Teacher() { Name = "Kreyszig", Id = 505}
                            }
                        }
                    },

                    new Period()
                    {
                        PeriodId = 2,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 6,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 7,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 8,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 9,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 10,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                        }
                    },
                    new Period()
                    {
                        PeriodId = 3,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 11,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 12,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 13,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 14,
                                SubjectName = "Music",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "A R Rehman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 15,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 4,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 16,
                                SubjectName = "Physics",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 17,
                                SubjectName = "Chemistry",
                                Location = "Room # 404",
                                Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 18,
                                SubjectName = "Math",
                                Location = "Room # 505",
                                Teacher = new Teacher() { Name = "Jonathan R", Id = 3}
                            },
                            new Lesson()
                            {
                                LessonId = 19,
                                SubjectName = "Art & Design",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Pablo Picasso", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 20,
                              SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 5,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 21,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 22,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 23,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 24,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 25,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 6,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 26,
                                SubjectName = "Physics",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 27,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 28,
                                SubjectName = "Math",
                                Location = "Room # 505",
                                Teacher = new Teacher() { Name = "Jonathan R", Id = 3}
                            },
                            new Lesson()
                            {
                                LessonId = 29,
                                SubjectName = "Art & Design",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Pablo Picasso", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 30,
                                SubjectName = "Biology",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Henry Gray", Id = 5}
                            }
                        }
                    }
                }
        };
        #endregion TUESDAY LIST

        #region WEDNESDAY
        private static SchoolDay wednesday = new SchoolDay()
        {
            DayName = DayOfWeek.Wednesday,
            Periods = new List<Period>()
                {
                    new Period()
                    {
                        PeriodId = 1,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 1,
                                SubjectName = "Physics",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 2,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 3,
                                SubjectName = "Math",
                                Location = "Room # 505",
                                Teacher = new Teacher() { Name = "Jonathan R", Id = 3}
                            },
                            new Lesson()
                            {
                                LessonId = 4,
                                SubjectName = "Computing",
                                Location = "Room # 313",
                                Teacher = new Teacher() { Name = "Scott Hanselman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 5,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },

                    new Period()
                    {
                        PeriodId = 2,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 6,
                                SubjectName = "Computer Programing",
                                Location = "Room # 110",
                                Teacher = new Teacher() { Name = "Robert Lafore", Id = 501}
                            },
                            new Lesson()
                            {
                                LessonId = 7,
                                SubjectName = "Circuit Analysis",
                                Location = "Room # 220",
                                Teacher = new Teacher() { Name = "William Hart", Id = 502}
                            },
                            new Lesson()
                            {
                                LessonId = 8,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 503}
                            },
                            new Lesson()
                            {
                                LessonId = 9,
                                SubjectName = "Reletional Database",
                                Location = "Room # 440",
                                Teacher = new Teacher() { Name = "Itzik Ben-Gan", Id = 504}
                            },
                            new Lesson()
                            {
                                LessonId = 10,
                                SubjectName = "Advanced Mathematics",
                                Location = "Room # 550",
                                Teacher = new Teacher() { Name = "Kreyszig", Id = 505}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 3,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 11,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 12,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 13,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 14,
                                SubjectName = "Music",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "A R Rehman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 15,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 4,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 16,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            },
                            new Lesson()
                            {
                                LessonId = 17,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 18,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 19,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 20,
                                SubjectName = "Computing",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Scott Hanselman", Id = 4}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 5,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 21,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 22,
                                SubjectName = "Music",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "A R Rehman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 23,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 24,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            },
                            new Lesson()
                            {
                                LessonId = 25,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                        }
                    },
                    new Period()
                    {
                        PeriodId = 6,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 26,
                                SubjectName = "Computing",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Scott Hanselman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 27,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 28,
                                SubjectName = "Mathematics",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 29,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            },
                            new Lesson()
                            {
                                LessonId = 30,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                        }
                    }
                }
        };
        #endregion WEDNESDAY

        #region THURSDAY
        private static SchoolDay thursday = new SchoolDay()
        {
            DayName = DayOfWeek.Thursday,
            Periods = new List<Period>()
                {
                    new Period()
                    {
                        PeriodId = 1,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 1,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 2,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 3,
                                SubjectName = "Mathematics",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 4,
                                SubjectName = "Computing",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Scott Hanselman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 5,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 2,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 6,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 7,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 8,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 9,
                                SubjectName = "Music",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "A R Rehman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 10,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 3,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 11,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 12,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 13,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 14,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 15,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 4,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 16,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 17,
                                SubjectName = "Design and technology",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Bill Gates", Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 18,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 19,
                                SubjectName = "Music",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "A R Rehman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 20,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 5,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 21,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 22,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 23,
                                SubjectName = "Mathematics",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 24,
                                SubjectName = "Computing",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Scott Hanselman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 25,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 6,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 26,
                                SubjectName = "Computer Programing",
                                Location = "Room # 110",
                                Teacher = new Teacher() { Name = "Robert Lafore", Id = 501}
                            },
                            new Lesson()
                            {
                                LessonId = 27,
                                SubjectName = "Circuit Analysis",
                                Location = "Room # 220",
                                Teacher = new Teacher() { Name = "William Hart", Id = 502}
                            },
                            new Lesson()
                            {
                                LessonId = 28,
                                SubjectName = "Digital Logic",
                                Location = "Room # 330",
                                Teacher = new Teacher() { Name = "Moris Mano", Id = 503}
                            },
                            new Lesson()
                            {
                                LessonId = 29,
                                SubjectName = "Reletional Database",
                                Location = "Room # 440",
                                Teacher = new Teacher() { Name = "Itzik Ben-Gan", Id = 504}
                            },
                            new Lesson()
                            {
                                LessonId = 30,
                                SubjectName = "Advanced Mathematics",
                                Location = "Room # 550",
                                Teacher = new Teacher() { Name = "Kreyszig", Id = 505}
                            }
                        }
                    }
                }
        };
        #endregion THURSDAY

        #region FridayList
        private static SchoolDay fridayList = new SchoolDay()
        {
            DayName = DayOfWeek.Friday,
            Periods = new List<Period>()
                {
                    new Period()
                    {
                        PeriodId = 1,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 1,
                                SubjectName = "Physics",
                                Location = "Room # 313",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 2,
                                SubjectName = "Chemistry",
                                Location = "Room # 414",
                                Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 3,
                                SubjectName = "Mathematics",
                                Location = "Room # 212",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 4,
                                SubjectName = "Computing",
                                Location = "Room # 333",
                                Teacher = new Teacher() { Name = "Scott Hanselman", Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 5,
                                SubjectName = "Physical Education",
                                Location = "Room # 616",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 2,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 6,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 7,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 8,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 9,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 10,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 3,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 11,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 12,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 13,
                                SubjectName = "Mathematics",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 14,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 15,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 4,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 16,
                                SubjectName = "History",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "David Attenborough", Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 17,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 18,
                                SubjectName = "Geography",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Iain Stewart", Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 19,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 20,
                                SubjectName = "Physical Education",
                                Location = "Room # 606",
                                Teacher = new Teacher() { Name = "Dwayne Johnson", Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 5,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 21,
                                SubjectName = "Computer Programing",
                                Location = "Room # 110",
                                Teacher = new Teacher() { Name = "Robert Lafore", Id = 501}
                            },
                            new Lesson()
                            {
                                LessonId = 22,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 502}
                            },
                            new Lesson()
                            {
                                LessonId = 23,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 503}
                            },
                            new Lesson()
                            {
                                LessonId = 24,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 504}
                            },
                            new Lesson()
                            {
                                LessonId = 25,
                                SubjectName = "Advanced Mathematics",
                                Location = "Room # 550",
                                Teacher = new Teacher() { Name = "Kreyszig", Id = 505}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 6,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 26,
                                SubjectName = "Religious Education",
                                Location = "Room # 909",
                                Teacher = new Teacher() { Name = "Saint George", Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 27,
                                SubjectName = "Foreign Language",
                                Location = "Room # 101",
                                Teacher = new Teacher() { Name = "Barak Obama", Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 28,
                                SubjectName = "Mathematics",
                                Location = "Room # 202",
                                Teacher = new Teacher() { Name = "Alan Davies", Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 29,
                                SubjectName = "Physics",
                                Location = "Room # 303",
                                Teacher = new Teacher() { Name = "Charlie C", Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 30,
                                SubjectName = "Chemistry",
                                Location = "Room # 404",
                                Teacher = new Teacher() { Name = "Micheal J", Id = 2}
                            },
                        }
                    }
                }
        };

        #endregion Friday list

        #region SATURDAY LIST
        private static SchoolDay saturday = new SchoolDay()
        {
            DayName = DayOfWeek.Saturday,
            Periods = new List<Period>()
                {
                    new Period()
                    {
                        PeriodId = 1,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 1,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 2,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 3,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty,Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 4,
                              SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 5,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name =string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 2,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 6,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 7,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 8,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 9,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 10,
                              SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 3,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 11,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 12,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name =string.Empty, Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 13,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 14,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 15,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 4,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 16,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 17,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty,Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 18,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 19,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 20,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 5,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 21,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 501}
                            },
                            new Lesson()
                            {
                                LessonId = 22,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 502}
                            },
                            new Lesson()
                            {
                                LessonId = 23,
                              SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 503}
                            },
                            new Lesson()
                            {
                                LessonId = 24,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 504}
                            },
                            new Lesson()
                            {
                                LessonId = 25,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 505}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 6,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 26,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty,Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 27,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 28,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 29,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 30,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 2}
                            },
                        }
                    }
                }

        };
        #endregion SATURDAY LIST

        #region SUNDAY LIST
        private static SchoolDay sundayList = new SchoolDay()
        {
            DayName = DayOfWeek.Sunday,
            Periods = new List<Period>()
                {
                    new Period()
                    {
                        PeriodId = 1,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 1,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 2,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 2}
                            },
                            new Lesson()
                            {
                                LessonId = 3,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty,Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 4,
                              SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 5,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name =string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 2,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 6,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 7,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 8,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 9,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 10,
                              SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 3,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 11,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 12,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name =string.Empty, Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 13,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 14,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 15,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 4,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 16,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 6}
                            },
                            new Lesson()
                            {
                                LessonId = 17,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty,Id = 7}
                            },
                            new Lesson()
                            {
                                LessonId = 18,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 8}
                            },
                            new Lesson()
                            {
                                LessonId = 19,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 4}
                            },
                            new Lesson()
                            {
                                LessonId = 20,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 5}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 5,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 21,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 501}
                            },
                            new Lesson()
                            {
                                LessonId = 22,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 502}
                            },
                            new Lesson()
                            {
                                LessonId = 23,
                              SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 503}
                            },
                            new Lesson()
                            {
                                LessonId = 24,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 504}
                            },
                            new Lesson()
                            {
                                LessonId = 25,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 505}
                            }
                        }
                    },
                    new Period()
                    {
                        PeriodId = 6,
                        Lessons = new List<Lesson>()
                        {
                            new Lesson()
                            {
                                LessonId = 26,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty,Id = 101}
                            },
                            new Lesson()
                            {
                                LessonId = 27,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 102}
                            },
                            new Lesson()
                            {
                                LessonId = 28,
                               SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 103}
                            },
                            new Lesson()
                            {
                                LessonId = 29,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 1}
                            },
                            new Lesson()
                            {
                                LessonId = 30,
                                SubjectName = string.Empty,
                                Location = string.Empty,
                                Teacher = new Teacher() { Name = string.Empty, Id = 2}
                            },
                        }
                    }
                }

        };
        #endregion SUNDAY LIST
        public static Lesson UpdateLesson(string teacherName, string subjectName, string locationName, int periodId, int lessonId, DayOfWeek dayName)
        {
            switch (dayName)
            {
                case DayOfWeek.Monday:
                    {
                        Lesson lesson = GetMondayPeriods().Periods.First(p => p.PeriodId == periodId).Lessons.First(l => l.LessonId == lessonId);
                        lesson = UpdateLesson(lesson, teacherName, subjectName, locationName);
                        return lesson;
                    }
                case DayOfWeek.Tuesday: 
                    {
                        Lesson lesson = GetTuesdayPeriods().Periods.First(p => p.PeriodId == periodId).Lessons.First(l => l.LessonId == lessonId);
                        lesson = UpdateLesson(lesson, teacherName, subjectName, locationName);
                        return lesson;
                    }
                case DayOfWeek.Wednesday:
                    {
                        Lesson lesson = GetWednesdayPeriods().Periods.First(p => p.PeriodId == periodId).Lessons.First(l => l.LessonId == lessonId);
                        lesson = UpdateLesson(lesson, teacherName, subjectName, locationName);
                        return lesson;
                    }
                case DayOfWeek.Thursday:
                    {
                        Lesson lesson = GetThursdayPeriods().Periods.First(p => p.PeriodId == periodId).Lessons.First(l => l.LessonId == lessonId);
                        lesson = UpdateLesson(lesson, teacherName, subjectName, locationName);
                        return lesson;
                    }
                case DayOfWeek.Friday:
                    {
                        IEnumerable<Period> periods = GetFridayPeriods().Periods;
                        Lesson lesson = periods.First(p => p.PeriodId == periodId).Lessons.First(l => l.LessonId == lessonId);
                        lesson = UpdateLesson(lesson, teacherName, subjectName, locationName);
                        return lesson;
                    }
                case DayOfWeek.Saturday:
                    {
                        IEnumerable<Period> periods = GetSaturdayPeriods().Periods;
                        Lesson lesson = periods.First(p => p.PeriodId == periodId).Lessons.First(l => l.LessonId == lessonId);
                        lesson = UpdateLesson(lesson, teacherName, subjectName, locationName);
                        return lesson;
                    }
                case DayOfWeek.Sunday:
                    {
                        IEnumerable<Period> periods = GetSundayPeriods().Periods;
                        Lesson lesson = periods.First(p => p.PeriodId == periodId).Lessons.First(l => l.LessonId == lessonId);
                        lesson = UpdateLesson(lesson, teacherName, subjectName, locationName);
                        return lesson;
                    }
                default:
                    {
                        throw new ArgumentOutOfRangeException("Invalid day.");
                    }
            }
        }

        private static Lesson UpdateLesson(Lesson lesson, string teacherName, string subjectName, string locationName)
        {
            lesson.Location = locationName;
            lesson.Teacher.Name = teacherName;
            lesson.SubjectName = subjectName;

            return lesson;
        }

        public static SchoolDay GetMondayPeriods()
        {
            return monday;
        }

        public static SchoolDay GetTuesdayPeriods()
        {
            return tuesday;
        }
        
        public static SchoolDay GetWednesdayPeriods()
        {
            return wednesday;
        }

        public static SchoolDay GetThursdayPeriods()
        {
            return thursday;
        }
        
        public static SchoolDay GetFridayPeriods()
        {
            return fridayList;
        }

        public static SchoolDay GetSaturdayPeriods()
        {
            return saturday;
        }

        public static SchoolDay GetSundayPeriods()
        {
            return sundayList;
        }
    }
}