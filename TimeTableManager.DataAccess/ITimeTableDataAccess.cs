﻿using System;
using System.Collections.Generic;
using TimeTableManager.DomainModel;

namespace TimeTableManager.DataAccess
{
    public interface ITimeTableDataAccess
    {
        IEnumerable<SchoolDay> GetAllSchoolDays();

        SchoolDay GetSchoolDayFor(DayOfWeek dayOfWeek);

        Lesson UpdateLesson(string teacherName, string subjectName, string locationName, int periodId, int lessonId, DayOfWeek dayName);

    }
}