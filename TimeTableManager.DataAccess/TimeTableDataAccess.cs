﻿using System;
using System.Collections.Generic;
using TimeTableManager.DomainModel;

namespace TimeTableManager.DataAccess
{
    public class TimeTableDataAccess : ITimeTableDataAccess
    {
        public Lesson UpdateLesson(string teacherName, string subjectName, string locationName, int periodId, int lessonId, DayOfWeek dayName)
        {
            return TimeTableRepository.UpdateLesson(teacherName, subjectName, locationName, periodId, lessonId, dayName);
        }


        public IEnumerable<SchoolDay> GetAllSchoolDays()
        {
            IList<SchoolDay> schoolDays = new List<SchoolDay>();
            schoolDays.Add(TimeTableRepository.GetMondayPeriods());
            schoolDays.Add(TimeTableRepository.GetTuesdayPeriods());
            schoolDays.Add(TimeTableRepository.GetWednesdayPeriods());
            schoolDays.Add(TimeTableRepository.GetThursdayPeriods());
            schoolDays.Add(TimeTableRepository.GetFridayPeriods());

            schoolDays.Add(TimeTableRepository.GetSaturdayPeriods());
            schoolDays.Add(TimeTableRepository.GetSundayPeriods());
            return schoolDays;
        }

        public SchoolDay GetSchoolDayFor(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                {
                    return TimeTableRepository.GetMondayPeriods();
                }

                case DayOfWeek.Tuesday:
                
                {
                    return TimeTableRepository.GetTuesdayPeriods();
                }

                case DayOfWeek.Wednesday:
                {
                    return TimeTableRepository.GetWednesdayPeriods();
                }

                case DayOfWeek.Thursday:
                {
                    return TimeTableRepository.GetThursdayPeriods();
                }
                case DayOfWeek.Friday:
                {
                    return TimeTableRepository.GetFridayPeriods();
                }
                case DayOfWeek.Saturday:
                {
                    return TimeTableRepository.GetSaturdayPeriods();
                }
                case DayOfWeek.Sunday:
                {
                    return TimeTableRepository.GetSundayPeriods();
                }
                default:
                {
                    throw new ArgumentOutOfRangeException("dayOfWeek");
                }
            }
        }

    }
}
